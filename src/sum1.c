/**
 * Demonstracija vzporednega seštevanja
 * 
 * # vzporedno (pametno!?) seštevanje z le enim bufferjem
 * gcc -O3 -s sum1.c -fopenmp -o sum1; time ./sum1
 * # lahko ga primerjamo z sekvenčno izvedbo istega algoritma
 * gcc -O3 -s sum1.c          -o sum1; time ./sum1
 * */


#include <stdio.h>
#include <stdint.h>
#define n (1024*1024)		// sum = 549755289600
#define NUM_REPEATS 1000L

// suma = sum(a);
long suma = 0;
// actual arrays
volatile long a[n];

int main () {
  for(int i=0; i<n; i++) { 
    a[i] = i; 
  }
  for(long rep=0; rep<NUM_REPEATS; rep++) {	  
	  suma=0;
    // a[0]=a[0]+a[1], b[1]=a[2]+a[3], ...
    for (int dynamic_n=n; dynamic_n>1; dynamic_n/=2) { 
      #pragma omp parallel for
      for(int i=0; i<dynamic_n; i+=2) { 
        a[i/2] = a[i]+a[i+1]; 
      }
    }
    suma = a[0];
  }

  printf("%ld\n", suma);  
}
