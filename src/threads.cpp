/**
 * Demonstracija (ne)varne uporabe niti
 * 
 * g++ -O3 -s threads.cpp -lpthread && time ./a.out
 */

#include <iostream>
#include <thread>
#include <vector>
#include <mutex>

constexpr int numThreads = 2;

/// global variables for all threads to work on
int counter = 0;
std::vector<int> vec;
// mutex to lock the critical region
std::mutex mylock;

/**
 * @brief This function will increment counter and push its value to vec
 * 
 * @param num 
 */
void push2vector(int num) {
	try {
		for (int i = 0; i < num; i++){
      /// a lock here will cause the behaviour to be correct
//			mylock.lock();
			counter++;
      // a lock here wil just protect the access to vector
//			mylock.lock();
			vec.push_back(counter);
      // unlock or create a dead-lock
//			mylock.unlock();
		}
	}
	catch (std::exception e) {
		std::cout << "counter = " << counter << std::endl;
		std::cerr << e.what() << std::endl;
	}
	
}

int main() {
  const std::vector<int> nList{100, 1000, 10000, 100'000, 1'000'000, 10'000'000};
  const std::vector<int> nt{1, 2, 3, 4, 10, 20, 100};

  // loop through list of possible n and list of number of threads
  for (int numThreads : nt) {
    for (int n : nList) {
      // initialize global variables vec and counter for each iteration to start fresh
      counter = 0;
      std::vector<int> emptyVector;
      vec.swap(emptyVector);
      /// ensure safe (and fast) use of vector
      vec.reserve(n);

      // create and start a number of threads 
      std::vector<std::thread> threads;
      for (int j = 0; j < numThreads; ++j)
        threads.push_back(std::thread(push2vector, n/numThreads));

      // wait for all the threads to finish
      for (int j = 0; j < numThreads; ++j)
        threads[j].join();

      // check for repeated values in vector (there should be none)
      bool error = false;
      for (int j = 0; j < vec.size() - 1; j++) {
        error |= (vec[j] > vec[j + 1]);
      }
      // print the results to std output
      std::cout << "n = " << n << ", " << numThreads << " threads: " << (error ? "FAIL" : "ok") << std::endl;
    }
  }
}