/**
 * Demonstracija avtomatske uporabe vektorskih operacij (MMX, SSE, AVX ipd)
 * 
 * # compare vectorized version
 * gcc -ftree-vectorize -fopt-info-vec -O3 -s vector_op.c; time ./a.out
 * # to the non-vectorized version
 * gcc -fno-tree-vectorize -fopt-info-vec -O3 -s vector_op.c -o b.out; time ./b.out
 * */

#include <stdio.h>
#define n 1024
#define NUM_REPEATS 10000000L

int main () {
  int a[n], b[n], c[n];

  for(long rep=0; rep<NUM_REPEATS; rep++) {
    for(int i=0; i<n; i++) { 
        a[i] = i; 
        b[i] = i*i; 
    }
    for(int i=0; i<n; i++) 
      c[i] = a[i]+b[i];
  }

  printf("%d\n", c[n-1]);  
}
