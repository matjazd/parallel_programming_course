/**
 * Demonstracija ucinkovitosti bitnih operacij
 * 
 * # compare vectorized version
 * g++ -ftree-vectorize -fopt-info-vec -O3 -s bit_op.cpp && time ./a.out
 * # to the non-vectorized version
 * gcc -fno-tree-vectorize -O3 -s bit_op.cpp && time ./a.out
 * */

#include <iostream>
#include <array>
#include <random>
#define n 1024000
#define NUM_REPEATS 100L
#define TYPE char
#define TYPE_SIZE 1

int main () {
  std::array<TYPE, n/TYPE_SIZE> a, b, c;
  std::mt19937 gen(1); 
  std::uniform_int_distribution<> distrib(0, 1);

  for(long rep=0; rep<NUM_REPEATS; rep++) {
    for(int i=0; i<n/TYPE_SIZE; i++) { 
      TYPE temp_a = 0;
      TYPE temp_b = 0;
      for(int j=0; j<TYPE_SIZE; ++j) {
        temp_a = (TYPE)distrib(gen) << j;
        temp_b = (TYPE)distrib(gen) << j;
      }
      a[i] = temp_a;
      b[i] = temp_b;
    }
    for(int i=0; i<n/TYPE_SIZE; i++) 
      c[i] = a[i] | b[i];
  }

  std::cout << c[n/TYPE_SIZE-1] << "\n";  
}
