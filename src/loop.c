/**
 * Demonstracija avtomatske uporabe vektorskih operacij (MMX, SSE, AVX ipd)
 * 
 * # compare the truly sequential version
 * gcc -fno-tree-vectorize -fopt-info-vec -O3 -s loop.c; time ./a.out
 * # to the vectorized version
 * gcc -ftree-vectorize -fopt-info-vec -O3 -s loop.c -o b.out; time ./b.out
 * # to the thread-parallel version
 * gcc -fno-tree-vectorize -fopt-info-vec -O3 -s loop.c -fopenmp -o c.out; time ./c.out
 * # to the thread-parallel and vectorized version
 * gcc -ftree-vectorize -fopt-info-vec -O3 -s loop.c -fopenmp -o d.out; time ./d.out
 * */


#include <stdio.h>
#include <stdint.h>
#define n 102400
#define NUM_REPEATS 10000L

int a[n], b[n], c[n];

int main () {  
  for(long rep=0; rep<NUM_REPEATS; rep++) {
	#pragma omp parallel for
    for(int i=0; i<n; i++) { 
        a[i] = i; 
        b[i] = i*i; 
    }
    #pragma omp parallel for
    for(int i=0; i<n; i++) 
      c[i] = a[i]+b[i];
  }

  printf("%d\n", (int)c[n-1]);  
}

