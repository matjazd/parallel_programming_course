/**
 * Demonstracija avtomatske uporabe vektorskih operacij (MMX, SSE, AVX ipd)
 * 
 * # compare the truly sequential version
 * gcc -fno-tree-vectorize -fopt-info-vec -O3 -s matrix.c; time ./a.out
 * # to the vectorized version
 * gcc -ftree-vectorize -fopt-info-vec -O3 -s matrix.c -o b.out; time ./b.out
 * # to the thread-parallel version
 * gcc -fno-tree-vectorize -fopt-info-vec -O3 -s matrix.c -fopenmp -o c.out; time ./c.out
 * # to the thread-parallel and vectorized version
 * gcc -ftree-vectorize -fopt-info-vec -O3 -s matrix.c -fopenmp -o d.out; time ./d.out
 * */

#include <stdio.h>
#include <stdint.h>
#define n 1024
#define NUM_REPEATS 1000L

int a[n][n], b[n][n], c[n][n];
  
int main () {  
  for(long rep=0; rep<NUM_REPEATS; rep++) {
	#pragma omp parallel for
    for(int i=0; i<n; i++) { 
	  for(int j=0; j<=i; j++) { 
        a[i][j] = i+j; 
        b[i][j] = i*j; 
	  }
    }
    #pragma omp parallel for
    for(int i=0; i<n; i++) 
      for(int j=0; j<=i; j++) 
        c[i][j] = a[i][j]+b[i][j];
  }

  printf("%d\n", (int)c[n-1][n-1]);  
}
