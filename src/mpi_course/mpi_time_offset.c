#include <mpi.h>
#include <stdio.h>

int main(int argc, char **argv) {
    int rank, size, sLen;
    char name[MPI_MAX_PROCESSOR_NAME], otherName[MPI_MAX_PROCESSOR_NAME];
    int tag=0xf00d, repeats=1000;
    double timeStamp, timeStart, timeEnd, minCommTime, timeOffset;
    MPI_Status status;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Get_processor_name(name, &sLen);
       
    if (rank == 0) {
		for (int p = 1; p < size; ++p) {
			for (int i = 0; i < repeats; ++i) {
				timeStart = MPI_Wtime();
				MPI_Send(&timeStamp, 1, MPI_DOUBLE, p, tag, MPI_COMM_WORLD);
				MPI_Recv(&timeStamp, 1, MPI_DOUBLE, p, tag, MPI_COMM_WORLD, &status);
				timeEnd = MPI_Wtime();
				if ((i == 0) || (timeEnd - timeStart < minCommTime)) {
					minCommTime = timeEnd - timeStart;
					timeOffset = timeEnd - timeStamp;
				}
			}
			MPI_Recv(otherName, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, p, tag, MPI_COMM_WORLD, &status);
			
			printf("Time offset between %s and %s: %f ms\n", name, otherName, timeOffset*1000);
			printf("(Timer resolution is %f ms\n", MPI_Wtick()*1000);
		}
	} else {
        for (int i = 0; i < repeats; ++i) {
            MPI_Recv(&timeStamp, 1, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD, &status);
            timeStamp = MPI_Wtime();
            MPI_Send(&timeStamp, 1, MPI_DOUBLE, 0, tag, MPI_COMM_WORLD);
        }
        MPI_Send(name, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, 0, tag, MPI_COMM_WORLD);
	}
        
    MPI_Finalize();
}


/*
 * merjenje razlike v času ki ga javi Wtime med posameznimi računalniki
 * */
