## Parallel processing with MPI

Remember, MPI can be used on a single machine as well as on a set of interconnected machines (e.g. by etherenet).

`vaja1.c` Introduction: MPI_Init, MPI_Finalize, MPI_Comm_rank, MPI_Comm_size, MPI_Set_processor_name.
`vaja2.c` MPI_Send & MPI_Recv in function of peer-to-peer communication; the possibilities of dead-lock. 
Noe that unless message size is large enough, send and receive will not be blocking. This is implementation dependant though.
`vaja3.c` Introducing MPI_Isend, MPI_Irecv, MPI_Waitall as a replacement for peer-to-peer communication.
`vaja4.c` Back to ordinary receive and send, this time using them in a way that eliminates the chance of dead-lock.
`vaja5.c` Measuring the time with MPI_Wtime.
`vaja6.c` Measuring performance.

`count_primes.c`  Collective communication (MPI_Barrier, MPI_Reduce) 
`count_primes2.c` A far better search for primes but this one contains a non-paralelizable part.
`count_primes2equivalent` As above buth with a different strategy for attacking the sequential part.
`count_primes3.c` Search for primes using Seive of Eratostenes
`count_primes3equivalent` As above but with a different strategy.

TODO: others