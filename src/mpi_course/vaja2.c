#include <mpi.h>
#include <stdio.h>


int main(int argc, char **argv) {
    int rank, size;
    int source, dest, tag=0xf00d;
    int sendBuf, recvBuf;
    MPI_Status status;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    source = (rank - 1) % size;
    dest = (rank + 1) % size;
    
    sendBuf = 10*rank;
    recvBuf = 0;
    
    MPI_Send (&sendBuf, 1, MPI_INT, dest,   tag, MPI_COMM_WORLD);
    MPI_Recv (&recvBuf, 1, MPI_INT, source, tag, MPI_COMM_WORLD, &status);
    
    printf("Process %3i sent %5i and received %5i\n", rank, sendBuf, recvBuf);
        
    MPI_Finalize();
}


/*
 * Sinhrona komunikacija z uporabo asinhronih funkcij
 * */
