#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char** argv) {
    int rank, size, tag_fail = 0xbad, tag_success = 0xcce;
    MPI_Status status;
    MPI_Request request, *retRequest;
    
    int done, nValues, luckyProcess = -1;
    int vecSize = 400000;
    int* vec;
    int i, iMax, j, result;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    retRequest = malloc(size * sizeof(MPI_Request));
    vec = malloc(vecSize * sizeof(int));
    if (rank == 0) {
        memset(vec, 0, sizeof(int)*vecSize);
        vec[1987654321 % vecSize] = 42;
    }
    
    MPI_Bcast(vec, vecSize, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Irecv(&result, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &request);
    nValues = (vecSize+size-1)/size;
    i = rank*nValues;
    iMax = i + nValues;
    if (iMax > vecSize)
        iMax = vecSize;
    
    for (; i < iMax; ++i) {
        MPI_Test(&request, &done, &status);
        if (done) break;
        
        if (vec[i] == 42) {
            luckyProcess = rank;
            for (j=0; j<size; ++j) {
                if (j != rank)
                    MPI_Isend(&i, 1, MPI_INT, j, tag_success, MPI_COMM_WORLD, &retRequest[j]);
                else 
                    retRequest[j] = MPI_REQUEST_NULL;
            }
            MPI_Waitall(size, retRequest, MPI_STATUSES_IGNORE);
            break;
        }
    };
    
    if (rank == 0) {
        if (!done)
            MPI_Wait(&request, &status);
                
        for (j = 1; j < size; ++j) {
            if (j > 1) {
                MPI_Irecv(&result, 1, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &request);
                MPI_Wait(&request, &status);
            }
            if (status.MPI_TAG == tag_success) {
                i = result;
                luckyProcess = status.MPI_SOURCE;
            }
        }
        printf("P:%d found it at index %d\n", luckyProcess, i);         fflush(stdout);
    } else {
        if (luckyProcess != rank) {
            MPI_Isend(&i, 1, MPI_INT, 0, tag_fail, MPI_COMM_WORLD, &retRequest[j]);
        } else {
            MPI_Request_free(&request);
        }
    }
    
    free(retRequest);
    free(vec);
    MPI_Finalize();
}


/*
 * Asinhrona komunikacija + Braodcast
 * Isend in Irecv
 * Wait, Waitall in Test pa so pomožne funkcije za asinhrono komunikacijo
 * */
