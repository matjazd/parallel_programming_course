#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>


bool isPrime(int n, int* list, int* listSize, int listMaxSize) {
	if ((n & 1) == 0)
		return false;
		
	for (int i = 0; (i < *listSize) && (list[i]*list[i] <= n); i++)
		if ((n % list[i]) == 0)
			return false;
			
	if (*listSize < listMaxSize) {
        list[*listSize] = n;
		(*listSize)++;
	}
	return true;
}


int main(int argc, char **argv) {
    int rank, size, count;
    int tag=0xf00d, tableSize, tableInit;
    double timeStart, timeEnd, timeMid;
    MPI_Status status;
    int* listOfPrimesPart;
    int* listOfPrimes;
    long* table;
    long numberLimit;
    int numPrimes, numPrimesCommon, maxNumPrimes;
    
	timeStart = MPI_Wtime();
    
    // MPI init and query
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    // arguments to program
    if (argc > 1)
        tableSize = atoi(argv[1]);
    else
        tableSize = 200000;
        
    if (argc > 2)
        tableInit = atoi(argv[2]);
    else
        tableInit = 1;
        
    numberLimit = ((1l << 48) - 1); // will only check integers up to this limit
    
    // random table of integers to check
    table = malloc(tableSize*sizeof(long));
    srand(tableInit);
    for (int i = 0; i < tableSize; ++i)
        table[i] = (rand() + (rand() << 16) + ((long)rand() << 32)) & numberLimit;

    // generate primes in parallel
    /*
     * all processes generate first sqrt(numberLimit) primes and store them in listOfPrimes
     * all processes generate their part of total prime range (up to numberLimit) and store them in in listOfPrimesPart
     * processes gather into listOfPrimes their parts
     * listOfPrimes is sorted
     * */
    numberLimit = sqrt(numberLimit);
	maxNumPrimes = (500+numberLimit/10)*sizeof(int);
	listOfPrimes = malloc(maxNumPrimes);
    listOfPrimesPart = malloc(maxNumPrimes);
	listOfPrimes[0] = 3;
	numPrimes = 1;
	
    int numSharedPrimes = (int)sqrt(numberLimit);
    int numSearched = (numberLimit-numSharedPrimes+size-1) / size;
    int firstSearched = numSharedPrimes+1+rank*numSearched;
    if ((firstSearched & 1) == 0)
		++firstSearched;
    int lastSearched = numSharedPrimes+1+(rank+1)*numSearched;
    if (lastSearched > numberLimit+1)
		lastSearched = numberLimit+1;
    
    for (int i = 3; i < numSharedPrimes; i += 2) 
        isPrime(i, listOfPrimes, &numPrimes, numSharedPrimes+1);
    for (int i = 0; i < numPrimes; ++i)
        listOfPrimesPart[i] = listOfPrimes[i];
    numPrimesCommon = numPrimes;
    for (int i = firstSearched; i < lastSearched; i += 2)
		isPrime(i, listOfPrimesPart, &numPrimes, maxNumPrimes);
    count = numPrimes - numPrimesCommon;
    
    // gather table of primes
    int* listSizes = malloc(size*sizeof(int));
    int* displacements = malloc(size*sizeof(int));
    MPI_Allgather(&count, 1, MPI_INT, listSizes, 1, MPI_INT, MPI_COMM_WORLD);
    displacements[0] = 0;
    for (int i = 1; i < size; ++i)
        displacements[i] = listSizes[i-1] + displacements[i-1];
    numPrimes = displacements[size-1] + listSizes[size-1];
    MPI_Allgatherv(&listOfPrimesPart[numPrimesCommon], count, MPI_INT, &listOfPrimes[numPrimesCommon], listSizes, displacements, MPI_INT, MPI_COMM_WORLD);
    free(listSizes);
    free(displacements);
    free(listOfPrimesPart);
    
    timeMid = MPI_Wtime();
    
    // now primes are stored in listOfPrimes, the number of them in numPrimes; test the table for primes
    int iStart = (tableSize *  rank   ) / size;
    int iEnd   = (tableSize * (rank+1)) / size;
    count = 0;
    for (int i = iStart; i < iEnd; ++i) {
        if (isPrime(table[i], listOfPrimes, &numPrimes, 0))
            ++count;
    }
    int countAll;
    MPI_Reduce(&count, &countAll, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    timeEnd = MPI_Wtime();
    if (rank == 0)	{
		printf("Counted %d primes in random generated list of size %d, with random seed %d.\n", countAll, tableSize, tableInit);
        printf("Generating primes took %f, and testing took %f s.\n", (timeMid - timeStart), (timeEnd - timeMid));
	}
	
	free(listOfPrimes);
        
    MPI_Finalize();
}


/*
 * program v dveh delih
 * 1. poišče praštevila do sqrt(2^31) 
 * 2. za podano tabelo števil preveri, katera so praštevila.
 * MPI_Allgather
 * MPI_Allgatherv
 * */
