#include <mpi.h>
#include <stdio.h>

int main(int argc, char **argv) {
    int rank, size, sLen;
    int source, dest, count, tag=0xf00d;
    MPI_Status status;
    double timeStart, timeCommStart, timeEnd;
    
    timeStart = MPI_Wtime();
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    timeCommStart = MPI_Wtime();
    source = (rank - 1) % size;
    dest = (rank + 1) % size;
    if (rank == 0) {
		count = 1;
		MPI_Send(&count, 1, MPI_INT, dest,   tag, MPI_COMM_WORLD);
		MPI_Recv(&count, 1, MPI_INT, source, tag, MPI_COMM_WORLD, &status);
		
		printf("The result of the count = %i\n", count);
	} else {
		MPI_Recv(&count, 1, MPI_INT, source, tag, MPI_COMM_WORLD, &status);
		count += 1;
		MPI_Send(&count, 1, MPI_INT, dest,   tag, MPI_COMM_WORLD);
	}
    timeEnd = MPI_Wtime();
    printf("Process %2d timers: %f  %f  %f \n", rank, timeCommStart-timeStart, timeEnd-timeCommStart, timeEnd-timeStart);
        
    MPI_Finalize();
}


/*
 * Merjenje časa
 * Wtime (wall clock time)
 * tudi Wtick, ki poda resolucijo ure (mikro sekunda na POSIX sistemih)
 * primerjava časov za initializacijo in komunikacijo
 * primerjava časov za komunikacijo na različnih procesorjih
 * */
