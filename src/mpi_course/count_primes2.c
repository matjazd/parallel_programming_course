#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


bool isPrime(int n, int* list, int* listSize, int listMaxSize) {
	if ((n & 1) == 0)
		return false;
		
	for (int i = 0; (i < *listSize) && (list[i]*list[i] <= n); i++)
		if ((n % list[i]) == 0)
			return false;
			
	if (*listSize < listMaxSize) {
		list[*listSize] = n;
		(*listSize)++;
	}
	return true;
}


int main(int argc, char **argv) {
    int rank, size, count;
    int tag=0xf00d, lastNum;
    double timeStart, timeEnd, timeSequential;
    MPI_Status status;
    int* listOfPrimes;
    int numPrimes, maxNumPrimes;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    if (argc > 1)
        lastNum = atoi(argv[1]);
    else
        lastNum = 200000;

	maxNumPrimes = lastNum/2*sizeof(int);
	listOfPrimes = malloc(maxNumPrimes);
	listOfPrimes[0] = 3;
	numPrimes = 1;
	
	// sequential algorithm (only executed on process 0)
    if (rank == 0) {
		timeStart = MPI_Wtime();
		
		count = 2; // 2 and 3 are primes
		for (int i = 5; i <= lastNum; i += 2) {
			count += (isPrime(i, listOfPrimes, &numPrimes, maxNumPrimes) ? 1 : 0);
		}
		
		timeEnd = MPI_Wtime();
		timeSequential = (timeEnd - timeStart);
		
		printf("Sequential count of primes between 2 and %12d took %f s and found %d primes.\n", lastNum, timeSequential, count);
	}
	
	MPI_Barrier(MPI_COMM_WORLD);
	numPrimes = 1;
    
    // parallel algorithm
	timeStart = MPI_Wtime();
    int numSearched = (lastNum-5+size) / size;
    int firstSearched = 5+rank*numSearched;
    if ((firstSearched & 1) == 0)
		++firstSearched;
    int lastSearched = 5+(rank+1)*numSearched;
    if (lastSearched > lastNum+1)
		lastSearched = lastNum+1;
	
	if (rank == 0)
		count = 2; // 2 and 3 are primes
	else 
		count = 0;
	
	// fill up the prime database (process 0 does this in its main search loop)
	if (rank != 0) {
		for (int i = 3; i*i < lastSearched; i += 2) {
			isPrime(i, listOfPrimes, &numPrimes, maxNumPrimes);
		}	
	}
	// search in the designated portion 
	for (int i = firstSearched; i < lastSearched; i += 2) {
		count += (isPrime(i, listOfPrimes, &numPrimes, maxNumPrimes) ? 1 : 0);
	}
	
	if (rank == 0) {
		int otherCount;
		for (int i = 1; i < size; ++i) {
			MPI_Recv(&otherCount, 1, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
			count += otherCount;
		}
	
		timeEnd = MPI_Wtime();
		
		printf("Equivalent parallel count took                             %f s and found %d primes.\n", (timeEnd - timeStart), count);
		printf("Speedup was %.2f\n", timeSequential / (timeEnd - timeStart));
	} else {
		MPI_Send(&count, 1, MPI_INT, 0, tag, MPI_COMM_WORLD);
	}
	
	free(listOfPrimes);
        
    MPI_Finalize();
}


/*
 * Preprost algoritem za iskanje praštevil (testira le deljivost s prastevili)
 * */
