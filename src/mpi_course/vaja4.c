#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    int rank, size;
    int source, dest, tag=0xf00d;
    int *buf, length = 10000; // length naj bo vsaj 2
    MPI_Status status;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    source = (rank - 1) % size;
    dest = (rank + 1) % size;
    
    buf = malloc(length * sizeof(int));
    
    if (rank == 0) {
		buf[0] = 1;
		buf[1] = 10*rank;
		MPI_Send(buf, length, MPI_INT, dest,   tag, MPI_COMM_WORLD);
		MPI_Recv(buf, length, MPI_INT, source, tag, MPI_COMM_WORLD, &status);
		printf("Process %3i sent %5i and received %5i\n", rank, 10*rank, buf[1]);
		
		printf("The result of the count = %i\n", buf[0]);
	} else {
		MPI_Recv(buf, length, MPI_INT, source, tag, MPI_COMM_WORLD, &status);
		printf("Process %3i received %5i and sent %5i\n", rank, buf[1], 10*rank);
		buf[0] += 1;
		buf[1] = 10*rank;
		MPI_Send(buf, length, MPI_INT, dest,   tag, MPI_COMM_WORLD);
	}
	
    
    free(buf);
        
    MPI_Finalize();
}


/*
 * Sinhrona komunikacija:
 * Send in Recv
 * obvezno v paru!
 * kaj se zgodi če se program požene na 1 procesorju?
 * */
