#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    int rank, size, sLen;
    char name[MPI_MAX_PROCESSOR_NAME], otherName[MPI_MAX_PROCESSOR_NAME];
    int tag=0xf00d, repeats, msgSize, total = 10000000;
    double timeStart, timeEnd;
    MPI_Status status;
    char* buffer;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Get_processor_name(name, &sLen);
    
    if (argc > 1)
        msgSize = atoi(argv[1]);
    else
        msgSize = 200;
        
    if (msgSize > total)
        msgSize = total;
        
    repeats = total/msgSize;
       
    buffer = malloc(total);
    if (rank == 0) {
		for (int p = 1; p < size; ++p) {
			printf("Communicating with process %d\n", p);
			timeStart = MPI_Wtime();
			for (int i = 0; i < repeats; ++i) {
				MPI_Send(buffer+i*msgSize, msgSize, MPI_CHAR, p, tag, MPI_COMM_WORLD);
				MPI_Recv(buffer+i*msgSize, msgSize, MPI_CHAR, p, tag, MPI_COMM_WORLD, &status);
			}
			timeEnd = MPI_Wtime();
			MPI_Recv(otherName, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, p, tag, MPI_COMM_WORLD, &status);
			
			printf("Measurements for message size of %d bytes transferred between %s and %s: transfer time = %f ms; bandwidth = %f MB/s\n", msgSize, name, otherName, (timeEnd - timeStart)*1000/(2*repeats), (2*total/1000000)/(timeEnd - timeStart));
		}
	} else {
        for (int i = 0; i < repeats; ++i) {
            MPI_Recv(buffer+i*msgSize, msgSize, MPI_CHAR, 0, tag, MPI_COMM_WORLD, &status);
            MPI_Send(buffer+i*msgSize, msgSize, MPI_CHAR, 0, tag, MPI_COMM_WORLD);
        }
        MPI_Send(name, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, 0, tag, MPI_COMM_WORLD);
	}
    free(buffer);
        
    MPI_Finalize();
}


/*
 * merjenje trajanja komunikacije
 * */
