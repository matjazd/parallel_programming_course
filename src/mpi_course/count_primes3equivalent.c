#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>


void initSieve(char** sieve, int sieveLength) {
	char* temp = malloc(sieveLength);
	for (int i = 3; i < sieveLength; ++i)
		temp[i] = (i & 1) == 0 ? false : true;
	temp[0] = temp[1] = false;
	temp[2] = true;
	*sieve = temp;
}

void sieveOutPrimes(char* sieve, int sieveLength) {
	for (int i = 3; i*i < sieveLength; i += 2) {
        if (sieve[i]) {
            for (int ii = i*i; ii < sieveLength; ii += 2*i) {
                sieve[ii] = false;
            }
        }
	}
}

void sieveOutPrimesInPart(char* sieve, int startNum, int endNum) {
	for (int i = 3; i*i < endNum; i += 2) {
        if (sieve[i]) {
            // find last multiple of i that is smaller than startNum
            int factor = (startNum-1) / i; 
            // find the first even number that is larget or equal to startNum, and is a multiple of i
            int firstC = (((factor+1) | 1) -1) * i + i;
            for (int ii = firstC; ii < endNum; ii += 2*i) {
                sieve[ii] = false;
            }
		}
	}
}

int countPrimes(char* sieve, int sieveLength) {
	int count = 2; // 2 and 3
	for (int i = 5; i < sieveLength; i += 2) {
		count += (sieve[i] ? 1 : 0);
	}
	return count;
}

int countPrimesInPart(char* sieve, int startNum, int endNum) {
	int count = 0;
	for (int i = (startNum & ~1)+1; i < endNum; i += 2) {
		count += (sieve[i] ? 1 : 0);
	}
	return count + ((startNum <= 2) && (endNum > 2));
}


int main(int argc, char **argv) {
    int rank, size, count;
    int tag=0xf00d, lastNum;
    double timeStart, timeEnd, timeSequential;
    MPI_Status status;
    char* sieve;
    int numPrimes;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    if (argc > 1)
        lastNum = atoi(argv[1]);
    else
        lastNum = 200000;

	numPrimes = lastNum+1;
	initSieve(&sieve, numPrimes);
	
	// sequential algorithm (only executed on process 0)
    if (rank == 0) {
		timeStart = MPI_Wtime();

		sieveOutPrimes(sieve, numPrimes);		
//		sieveOutPrimesOpti(sieve, numPrimes);
		count = countPrimes(sieve, numPrimes);
		
		timeEnd = MPI_Wtime();
		timeSequential = (timeEnd - timeStart);
		
		printf("Sequential count of primes between 2 and %12d took %f s and found %d primes.\n", lastNum, timeSequential, count);
	}
	
	MPI_Barrier(MPI_COMM_WORLD);
	free(sieve);
	initSieve(&sieve, numPrimes);
    
    // parallel algorithm
	timeStart = MPI_Wtime();

	int numSharedPrimes = (int)sqrt(lastNum);
    int numSearched = (lastNum-numSharedPrimes+size-1) / size;
    int firstSearched = numSharedPrimes+1+rank*numSearched;
    if ((firstSearched & 1) == 0)
		++firstSearched;
    int lastSearched = numSharedPrimes+1+(rank+1)*numSearched;
    if (lastSearched > lastNum+1)
		lastSearched = lastNum+1;
        
    //printf("%d, %d - %d\n", numSharedPrimes, firstSearched, lastSearched);
    	
	// all processes search over the shared part of the list	
	sieveOutPrimes(sieve, numSharedPrimes+1);
	
	// all processes sieve out their designated part 
	sieveOutPrimesInPart(sieve, firstSearched, lastSearched);
	count = countPrimesInPart(sieve, firstSearched, lastSearched);
	
	if (rank == 0) {
		int tempBuf[3];;
		count += countPrimesInPart(sieve, 1, firstSearched);
        //printf("%d primes in common area", countPrimesInPart(sieve, 1, firstSearched));
		MPI_Request* request = malloc(sizeof(MPI_Request)*size);
		for (int i = 1; i < size; ++i) {
			MPI_Recv(tempBuf, 3, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
			MPI_Irecv(&sieve[tempBuf[1]], tempBuf[2]-tempBuf[1], MPI_CHAR, status.MPI_SOURCE, tag, MPI_COMM_WORLD, &request[status.MPI_SOURCE]);
			count += tempBuf[0];
		}
		MPI_Waitall(size-1, &request[1], MPI_STATUS_IGNORE);
		free(request);
		timeEnd = MPI_Wtime();
		printf("Equivalent parallel count took                             %f s and found %d primes.\n", (timeEnd - timeStart), count);
		printf("Check for equivalence: %d\n", countPrimes(sieve, numPrimes));
		printf("Speedup was %.2f\n", timeSequential / (timeEnd - timeStart));
	} else {
		int tempBuf[3] = {count, firstSearched, lastSearched};
		MPI_Send(tempBuf, 3, MPI_INT, 0, tag, MPI_COMM_WORLD);
		MPI_Send(&sieve[firstSearched], lastSearched-firstSearched, MPI_CHAR, 0, tag, MPI_COMM_WORLD);
	}
	
	free(sieve);
        
    MPI_Finalize();
}


/*
 * Najbolj preprost algoritem za iskanje praštevil (edina optimizacija je, da ne preiskuje sodih števil)
 * MPI_Barrier
 * MPI_Probe
 * */
