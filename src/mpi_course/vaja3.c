#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    int rank, size, sLen;
    int source, dest, tag=0xf00d;
    int *sendBuf, *recvBuf, length = 10000;
    MPI_Status status[2];
    MPI_Request request[2];
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    
    source = (rank - 1) % size;
    dest = (rank + 1) % size;
    
    sendBuf = malloc(length * sizeof(int));
    recvBuf = malloc(length * sizeof(int));
    sendBuf[0] = 10*rank;
    recvBuf[0] = 0;
    
    MPI_Irecv(recvBuf, length, MPI_INT, source, tag, MPI_COMM_WORLD, &request[0]);
    MPI_Isend(sendBuf, length, MPI_INT, dest,   tag, MPI_COMM_WORLD, &request[1]);
    MPI_Waitall(2, request, status);
//  MPI_Recv (recvBuf, length, MPI_INT, source, tag, MPI_COMM_WORLD, &status);
    
    printf("Process %3i sent %5i and received %5i\n", rank, sendBuf[0], recvBuf[0]);
    
    free(sendBuf);
    free(recvBuf);
        
    MPI_Finalize();
}


/*
 * Sinhrona komunikacija z uporabo asinhronih funkcij
 * */
