#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

void delay(int millis) {
	double start = omp_get_wtime();
	for (; ; ) {
		double end = omp_get_wtime();
		if ((end - start) > (millis*0.001))
			break;
	}
}

int main (int argc, char **argv)  {
	char threads[200] = "[------------------------------------------------------------]";
	int maxNum, numThreads;
	
    if (argc > 1)
        numThreads = atoi(argv[1]);
    else
        numThreads = omp_get_num_threads();
        
    if (argc > 2)
        maxNum = atoi(argv[2]);
    else
        maxNum = 60;
        
	omp_set_num_threads(numThreads);
	
	double start = omp_get_wtime();
	#pragma omp parallel 
	{
		#pragma omp for schedule(dynamic)
		for (int i=0; i < maxNum; ++i) {
			delay((i*3)%maxNum);
			threads[1+i] = (char)('0'+omp_get_thread_num());
		}
	}
	double end = omp_get_wtime();
	
	printf("threads = %s\n", threads);
	printf("time taken = %.4f\n", (end - start));
	printf("time resolution = %f\n", omp_get_wtick());
}
