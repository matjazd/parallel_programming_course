/**
 * Demonstracija avtomatskega multi-thredinga (uporaba več jeder istega računalnika)
 * 
 * # compile
 * gcc -O3 -s omp_for.c; time ./a.out
 * # run
 * export OMP_NUM_THREADS=8 ; time ./a.out
 * 
 * # script:
 * for i in $(seq 8) ; do export OMP_NUM_THREADS=$i ; echo -n "$i : " ; /usr/bin/time -f "%e" ./a.out > /dev/null ; done
 * */

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#define n (1024*1014*100)   // 100M 
#define NUM_REPEATS 1

int main () {
  int *a = calloc(n, sizeof(int)), *b  = calloc(n, sizeof(int)), *c = calloc(n, sizeof(int));
  long sum;

  for(long rep=0; rep<NUM_REPEATS; rep++) {
    #pragma omp parallel for shared(a,b)
    for(int i=0; i<n; i++) { 
        a[i] = i; 
        b[i] = (i*7717L+727) % n;
    }
    #pragma omp parallel for shared(a,b,c)
    for(int i=0; i<n; i++) 
      c[i] = a[i]+b[i];
    
    sum = 0;
    for(int i=0; i<n; i++) 
      sum += c[i];
  }

  printf("lastc = %d, sum = %ld\n", c[n-1], sum); 
}
