#include <stdio.h>
#include <omp.h>

void function(int i, char* threads) {	
	#pragma omp parallel for
	for (int j=0; j < 6; ++j) {
		threads[(i*6)+j+1] = (char)('0'+omp_get_thread_num());
	}
}

int main (int argc, char **argv)  {
	char results[200] = "[------------------------------------------------------------]";
	char threads[200] = "[------------------------------------------------------------]";
	
	omp_set_num_threads(4);
	
	//#pragma omp parallel num_threads(2)
	{
		//#pragma omp for 
		for (int i=0; i < 10; ++i) {
			function(i, threads);
			/*
			#pragma omp for
			for (int j=0; j < 6; ++j) {
				threads[(i*6)+j+1] = (char)('0'+omp_get_thread_num());
			}
			*/
		}
	}
	
	printf("results = %s\n", results);
	printf("threads = %s\n", threads);
}
