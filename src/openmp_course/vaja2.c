#include <stdio.h>
#include <omp.h>

int main (int argc, char **argv)  {
	int id, numThreads;
	printf("This is the main thread of the program\n");

	#pragma omp parallel private (id)
	{
		id = omp_get_thread_num();
		numThreads = omp_get_num_threads();
		printf("This is thread #%d of %d\n", id, numThreads);
	}  
}
