#include <stdio.h>
#include <omp.h>

int main (int argc, char **argv)  {
	int id, numThreads;
	char sillyString[100] = " [oooooooooooooooooooooooooooooo] <- tu je 30 o-jev ";
	printf("This is the main thread of the program\n");
	printf("Silly string = '%s'\n", sillyString);
	
	#pragma omp parallel for private(id)
	for (int i = 0; i < 30; ++i) {
		id = omp_get_thread_num();
		numThreads = omp_get_num_threads();
//		printf("This is thread #%d of %d\n", id, numThreads);
		sillyString[2 + i] = (char)('0'+id);;
	}  
	
	printf("back to the main thread\n");
	printf("Silly string = '%s'\n", sillyString);
}
