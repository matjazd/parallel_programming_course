# Parallel processing with openmp

The examples are in c; c++ would be equivalent.

`vaja1.c`   Just shows `#pragma omp parallel` statement. 
Use export OMP_NUM_THREADS=X to set X threads (X is an integer)
`vaja2.c`   Private and shared declarations 
`vaja3.c`   As previous but without the private declaration
`vaja4.c`   Access to shared variables
`vaja5.c`   Access to shared variables shows which parts of a loop are processed by which thread
`vaja6.c`   Checking if a number is prime for a set of numbers; in a really inefficient way.
`vaja7.c`   As previous, but cunt the primes (use of reduction in a parallel loop)
`vaja8.c`   OMP can do more than parallel loops, i.e. sections.
`vaja9.c`   As previous but with more sections, also with a nowait clause.
`vaja10.c`  A more complex OMP, can a parallel loop contain another parallel loop?
`vaja11.c`  Timing execution in OMP.
`omp_for.c` A very basic speedup gained through th ues of parallel for loop
