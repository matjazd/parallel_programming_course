#include <stdio.h>
#include <omp.h>
#include <stdbool.h>


bool isPrime(long n) {
	if ((n & 1) == 0)
		return false;
		
	for (long i=3; i*i <= n; i += 2)
		if ((n % i) == 0)
			return false;
	return true;
}


int main (int argc, char **argv)  {
	int id, numThreads;
	char results[200] = "[------------------------------------------------------------]";
	char threads[200] = "[------------------------------------------------------------]";
	long numbers[100];
	
	printf("results = %s\n", results);
	printf("threads = %s\n", threads);
	
	for (int i = 0; i < 100; ++i)
		numbers[i] = 100000000000001l + 2*i;
	
	#pragma omp parallel for private(id) schedule(dynamic)
	for (int i = 0; i < 60; ++i) {
		id = omp_get_thread_num();
		numThreads = omp_get_num_threads();
		results[1 + i] = (isPrime(numbers[i]) ? '1' : '0');
		threads[1 + i] = (char)(id + '0');
	}  
	
	printf("results = %s\n", results);
	printf("threads = %s\n", threads);
}
