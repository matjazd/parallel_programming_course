#include <stdio.h>
#include <omp.h>

int main (int argc, char **argv)  {
	
	#pragma omp parallel
	{
		#pragma omp sections nowait
		{
			#pragma omp section
			{
				printf("Thread #%d is here\n", omp_get_thread_num());
			}
			#pragma omp section
			{
				printf("While thread #%d is here\n", omp_get_thread_num());
			}
		}
		
		#pragma omp for
		for (int i=0; i < 10; ++i) {
			printf("#%d is looping; ", omp_get_thread_num());
		}
		printf("\n");
	}
}
