#include <stdio.h>
#include <omp.h>

int main (int argc, char **argv)  {
	printf("This is the sequential part of the program\n");

	#pragma omp parallel
	{
		printf("This is the 'parallel' part of the program\n");
	}  
}
