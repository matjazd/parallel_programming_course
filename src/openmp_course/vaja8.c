#include <stdio.h>
#include <omp.h>

int main (int argc, char **argv)  {
	
	#pragma omp parallel sections
	{
		#pragma omp section
		printf("Thread #%d is here\n", omp_get_thread_num());
		#pragma omp section
		printf("While thread #%d is here\n", omp_get_thread_num());
	}
}
