import numpy as np
from psutil import cpu_count
import ray
from timeit import timeit

"""
This file contains an example of using ray remote calls to speedup calculations.
https://github.com/ray-project/ray

The function to run in parallel is embarasingly parallel:
- input 3-D matrix
- output 3-D matrix (same size as input)
- function: 
  - calculate the mean of each 1-D slice of input 
  - add the mean to all elements of the same 1-D slice
"""


num_cpus = cpu_count(logical=False)
ray.init(num_cpus=num_cpus, ignore_reinit_error=True)
print(f"Initialized ray with {num_cpus} cpus.")

# sequential functionality is defined by the follwoing 2 functions
def add_average(in_ary: np.ndarray, out_ary: np.ndarray, x_index: int, y_index: int):
    """
    Adds the mean of an array slice to each element of the slice and assigns this slice into an output array.
    """
    ary = in_ary[x_index, y_index]
    out_ary[x_index, y_index] = ary + np.mean(ary)
    
def compute_with_loop(input_array: np.ndarray) -> np.ndarray:
    """
    Applies the add_average() function to each 1-D slice of a 3-D array and returns the result. 
    This is the sequential algorithm.
    """
    output_array = np.full(shape=input_array.shape, fill_value=np.NaN)
    for x in range(input_array.shape[0]):
        for y in range(input_array.shape[1]):
            add_average(input_array, output_array, x, y)
    return output_array

# add_average has to be rewritten for ray, since it has to return output instead of storing it to a global output var
@ray.remote
def add_average_ray(in_ary: np.ndarray, x_index: int, y_index: int):
    """
    Adds the mean of an array slice to each element of the slice and returns the resulting 
    1-D array slice along with the slice's indices.
    """
    ary = in_ary[x_index, y_index]
    return (ary + np.mean(ary), x_index, y_index)

def compute_with_ray(input_array: np.ndarray) -> np.ndarray:
    """
    Applies the add_average_ray() function to each 1-D slice of a 3-D array and returns the result. 
    Each 1-D slice is sent to remote exection separately.
    The results (1-D slices) are collected as futures and combined back into a 3-D array.
    """
    output_array = np.full(shape=input_array.shape, fill_value=np.NaN)
    in_array_id = ray.put(input_array)
    futures = []
    for x in range(input_array.shape[0]):
        for y in range(input_array.shape[1]):
            futures.append(add_average_ray.remote(in_array_id, x, y))

    results = ray.get(futures)
    for result in results:
        slice_array = result[0]
        x_index = result[1]
        y_index = result[2]
        output_array[x_index, y_index] = slice_array
        
    return output_array

# to speed up ray, cluster computations into bigger chunks (remember )
@ray.remote
def add_average_ray2(in_ary: np.ndarray, x_index: int):
    """
    Takes 3-D array input and index of the 2-D slice to process, takes the mean of each 1-D array slice to each element 
    of the same slice and returns the resulting 2-D array along with the slice's indices.
    """
    ary = in_ary[x_index]
    ary_ret = np.copy(ary)
    for y in range(ary.shape[0]): 
        ary_ret[y] += np.mean(ary[y])
    return (ary_ret, x_index)

def compute_with_ray2(input_array: np.ndarray) -> np.ndarray:
    """
    Applies the add_average_ray() function to each 2-D slice of a 3-D array and returns the result. 
    Message passing is used for collecting the results.

    """
    output_array = np.full(shape=input_array.shape, fill_value=np.NaN)
    in_array_id = ray.put(input_array)
    futures = []
    for x in range(input_array.shape[0]):
        futures.append(add_average_ray2.remote(in_array_id, x))

    results = ray.get(futures)
    for result in results:
        x_index = result[1]
        output_array[x_index] = result[0]

    return output_array

# to speed up ray further, try shared memory access hack on numpy.ndarray
@ray.remote
def add_average_ray3(in_ary: np.ndarray, out_ary: np.ndarray, x_index: int):
    """
    Takes 3-D array input, the 3-D array for resulty, and index of the 2-D slice to process.
    Takes the mean of each 1-D array slice to each element of the same slice.
    It writes the results to the output array and returns nothing.

    It uses momory sharing through a numpy hack - this will only work if workers are executed on the same machine.
    """
    ary = in_ary[x_index]
    out_ary.flags.writeable = True  # hack that turns on memory sharing with ray
    for y in range(ary.shape[0]):
        out_ary[x_index][y] += np.mean(ary[y])

def compute_with_ray3(input_array: np.ndarray) -> np.ndarray:
    """
    Applies the add_average_ray() function to each 2-D slice of a 3-D array and returns the result. 
    Shared-memory is used for collecting the results.
    """
    output_array = np.copy(input_array)
    in_array_id = ray.put(input_array)
    out_array_id = ray.put(output_array)

    futures = []
    for x in range(input_array.shape[0]):
        futures.append(add_average_ray3.remote(in_array_id, out_array_id, x))

    results = ray.get(futures)

    return ray.get(out_array_id)

# #################################################################################
# perform the experiments
# #################################################################################
input_array = np.ones(shape=(100, 100, 100))

# sequential calculus
print('')
print('execution times:')
print('sequential algorithm:')
print(timeit('average_added_loop = compute_with_loop(input_array)', number=10, setup="from __main__ import compute_with_loop, input_array"))
# parallel, fine-grained
print('fine-grained parallel algorithm with message passing:')
print(timeit('average_added_ray = compute_with_ray(input_array)', number=2, setup="from __main__ import compute_with_ray, input_array"))
# parallel, coarse-grained
print('coarse-grained parallel algorithm with message passing')
print(timeit('average_added_ray = compute_with_ray2(input_array)', number=10, setup="from __main__ import compute_with_ray2, input_array"))
print('coarse-grained parallel algorithm with memory sharing')
print(timeit('average_added_ray = compute_with_ray3(input_array)', number=10, setup="from __main__ import compute_with_ray3, input_array"))


