# Parallel processing with python 6 ray

`process.py` This python script contains the sequential and parallel codes, which it also executes and times.

`ray_intro.ipynb` This is a jupyter notebook with a step by step quide to the very basic usage of ray.
`pi.ipynb` Computes pi via a stochastic simulation; also includes a runtime analysis with a speedup graph.
`maxClique.ipynb` An algorithm for finding maximum clique in a graph. The algorithm is evolved through several steps into faster and faster versions
