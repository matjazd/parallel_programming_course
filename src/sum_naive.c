/**
 * Demonstracija naivnega seštevanja (strogo zaporedne)
 * 
 * gcc -O3 -s sum_naive.c -o sum_naive; time ./sum_naive
 * */
 

#include <stdio.h>
#include <stdint.h>
#define n (1024*1024)		// sum = 549755289600
#define NUM_REPEATS 1000L

// suma = sum(a);
volatile long a[n];
long suma = 0;

int main () {  
  for(int i=0; i<n; i++) { 
    a[i] = i; 
  }
  for(long rep=0; rep<NUM_REPEATS; rep++) {
	  suma = 0;
    for(int i=0; i<n; i++) { 
	    suma += a[i]; 
    }
  }

  printf("%ld\n", suma);  
}
