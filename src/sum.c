/**
 * Demonstracija vzporednega seštevanja
 * 
 * # vzporedno seštevanje
 * gcc -O3 -s sum.c -fopenmp -o sum; time ./sum
 * # lahko ga primerjamo z sekvenčno izvedbo istega algoritma
 * gcc -O3 -s sum.c          -o sum; time ./sum
 * */
 

#include <stdio.h>
#include <stdint.h>
#define n (1024*1024)		// sum = 549755289600
#define NUM_REPEATS 1000L

// suma = sum(a);
volatile long *a, *b, *temp;
long suma = 0;
// actual arrays
volatile long a1[n], a2[n];

int main () {
  for(int i=0; i<n; i++) { 
    a1[i] = i; 
  }
  for(long rep=0; rep<NUM_REPEATS; rep++) {	  
	  suma=0;
    a=a1;
    b=a2;
    // b[0]=a[0]+a[1], b[1]=a[2]+a[3], ...
    for (int dynamic_n=n; dynamic_n>1; dynamic_n/=2) { 
      #pragma omp parallel for
      for(int i=0; i<dynamic_n; i+=2) { 
        b[i/2] = a[i]+a[i+1]; 
      }
      // swap a and b
      temp = a;
      a = b;
      b = temp;
    }
    suma = a[0];
  }

  printf("%ld\n", suma);  
}
