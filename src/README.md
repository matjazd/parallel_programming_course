## Source files

`bit_op.cpp` ... an example of the use of bitwise operations, e.g. implementation of efficient sets; currently showing no speedup
`loop.c` ... demo of automatic vectorization (MMX, SSE, AVX, etc) and openmp on a simple loop / linear data structure
`matrix.c` ... demo of automatic vectorization (MMX, SSE, AVX, etc) and openmp on a double loop / 2D matrix
`sum_naive.c` ... naive totally sequential sum of numbers from 1 to n
`sum.c` ... parallel sum of numbers from 1 to n, using two buffers
`sum1.c` ... parallel sum of numbers from 1 to n, using a single buffers
`threads.cpp` ... demo of proper thread use (critical sections)
`vector_op.c` ... demo of using vector operations (the use of compiler switches)

## About timing

The examples here are so simple they are not using any internal timing. External timing should suffice, e.g. on linux use `time a.out`.
Instead of the builtin `time` utility, `hyperfine` can be used (<https://github.com/sharkdp/hyperfine>), which provides some averaging and comparison out of the box.

